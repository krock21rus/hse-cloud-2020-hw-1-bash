#!/bin/bash
round() {
    # $1 is expression to round (should be a valid bc expression)
    # $2 is number of decimal figures (optional). Defaults to three if none given
    local df=${2:-3}
    printf '%.*f\n' "$df" "$(bc -l <<< "a=$1; if(a>0) a+=5/10^($df+1) else if (a<0) a-=5/10^($df+1); scale=$df; a/1")"
}
read n

sum=0
for (( c=1; c<=$n; c++ ))
do
  read val
  sum=$(( sum + val ))
done

round $(echo "scale=5; $sum/$n" | bc) # BC SCALE=3 ROUND IT DOWN!!!!